package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strings"

	"zh-code.com/grpc/app/common"
)

const redisKey string = "gRPC_BadImages"

func main() {
	common.InitRedis()
	existingTags := common.GetExistingImages()
	badimages := loadBadImages()

	client := &http.Client{}
	hubVersionLink := "https://registry.hub.docker.com/v2/repositories/library/golang/tags"
	for hubVersionLink != "" {
		resp, err := client.Get(hubVersionLink)
		if err != nil {
			log.Fatal(err)
		}
		var versions common.HubVersionV2
		err = json.NewDecoder(resp.Body).Decode(&versions)
		if err != nil {
			log.Fatal(err)
		}

		hubVersionLink = versions.Next

		for _, result := range versions.Results {
			for _, image := range result.Images {
				if image.Os == "linux" {
					if result.Name == "latest" || (!common.Contains(existingTags, result.Name) && !strings.Contains(result.Name, "beta") &&
						!common.Contains(badimages, result.Name)) {
						log.Println(fmt.Sprintf("Pushing image: %s", result.Name))
						cmd := exec.Command("bash", "-c", fmt.Sprintf("./app/push/pushimage.sh %s", result.Name))
						cmd.Stdout = os.Stdout
						cmd.Stderr = os.Stderr
						err = cmd.Run()
						if err != nil {
							log.Println(fmt.Sprintf("Failed to push %s: %s", result.Name, err.Error()))
							if result.Name != "latest" {
								badimages = append(badimages, result.Name)
								common.RedisClient.Set(redisKey, strings.Join(badimages, ","), 0).Result()
							}
						}
					}
				}
			}
		}
		resp.Body.Close()
	}
}

func loadBadImages() []string {
	var images []string
	imagestr, err := common.RedisClient.Get(redisKey).Result()
	if err == nil {
		images = strings.Split(imagestr, ",")
	}
	return images
}

func appendStringToFile(path, text string) {
	f, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err == nil {
		defer f.Close()
		f.WriteString(text)
	}
}
