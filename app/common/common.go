package common

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

//HubVersionV1 is docker hub version
type HubVersionV1 []struct {
	Layer string `json:"layer"`
	Name  string `json:"name"`
}

//HubVersionV2 is docker hub version
type HubVersionV2 struct {
	Count    int         `json:"count"`
	Next     string      `json:"next"`
	Previous interface{} `json:"previous"`
	Results  []struct {
		Name     string `json:"name"`
		FullSize int    `json:"full_size"`
		Images   []struct {
			Size         int64       `json:"size"`
			Digest       string      `json:"digest"`
			Architecture string      `json:"architecture"`
			Os           string      `json:"os"`
			OsVersion    string      `json:"os_version"`
			OsFeatures   string      `json:"os_features"`
			Variant      interface{} `json:"variant"`
			Features     string      `json:"features"`
		} `json:"images"`
		ID                  int         `json:"id"`
		Repository          int         `json:"repository"`
		Creator             int         `json:"creator"`
		LastUpdater         int         `json:"last_updater"`
		LastUpdaterUsername string      `json:"last_updater_username"`
		ImageID             interface{} `json:"image_id"`
		V2                  bool        `json:"v2"`
		LastUpdated         time.Time   `json:"last_updated"`
	} `json:"results"`
}

//GetExistingImages gets images uploaded
func GetExistingImages() []string {
	var existingTags []string
	client := &http.Client{}
	resp, err := client.Get("https://registry.hub.docker.com/v1/repositories/zhironghuang/grpc/tags")
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	var versions HubVersionV1
	err = json.NewDecoder(resp.Body).Decode(&versions)
	if err != nil {
		log.Fatal(err)
	}
	for _, v := range versions {
		existingTags = append(existingTags, v.Name)
	}
	return existingTags
}

//Contains Contains
func Contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
