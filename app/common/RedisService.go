package common

import (
	"os"
	"sync"

	"github.com/go-redis/redis"
)

//RedisClient RedisClient
var RedisClient *redis.Client

var once sync.Once

//InitRedis inits Redis Client
func InitRedis() {
	RedisClient = redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_ADDR"),
		Password: os.Getenv("REDIS_PASSWORD"),
		DB:       0,
	})
}
