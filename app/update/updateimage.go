package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"

	"zh-code.com/grpc/app/common"
)

func main() {
	existingTags := common.GetExistingImages()

	for _, version := range existingTags {
		log.Println(fmt.Sprintf("Updating image: %s", version))
		cmd := exec.Command("bash", "-c", fmt.Sprintf("./app/update/updateimage.sh %s", version))
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		cmd.Run()
	}
}
