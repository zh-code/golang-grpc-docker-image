#!/bin/sh

docker buildx use multibuilder
docker buildx build --cpuset-cpus 0 --platform linux/amd64,linux/arm64,linux/ppc64le,linux/s390x,linux/386,linux/arm/v7 --build-arg "grpcversion=$1" -t "zhironghuang/grpc:$1" -f ./app/update/Dockerfile . --push