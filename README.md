# Golang gRPC Docker Image

Ready to use Golang gRPC image

# Instructions

Reference the Docker image in your Dockerfile as 

```
# Start from the latest golang base image
FROM zhironghuang/grpc:latest
```

This will keep update with official golang image